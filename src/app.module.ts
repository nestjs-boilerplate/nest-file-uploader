import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UploadModule } from './upload/upload.module';
import { UploadController } from './upload/upload.controller';

@Module({
  imports: [UploadModule],
  controllers: [AppController, UploadController],
  providers: [AppService],
})
export class AppModule {}

import { File } from '../interfaces/file.interface';
import { nanoid } from 'nanoid';
import { extname } from 'path';

export const createFileName = (
  req: unknown,
  file: File,
  callback: (arg, name) => void,
): void => {
  const timestamp = new Date().getTime();
  const extension = extname(file.originalname);
  const id = nanoid(6);
  callback(null, `${timestamp}_${id}${extension}`);
};

import {
  Controller,
  Post,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { createFileName } from './utils/file-upload.utils';
import { File } from './interfaces/file.interface';

@Controller('upload')
export class UploadController {
  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: createFileName,
      }),
    }),
  )
  uploadFile(
    @UploadedFile() file: File,
  ): { originalname: string; filename: string } {
    const { originalname, filename } = file;
    return {
      originalname,
      filename,
    };
  }
}
